#include <iostream>
#include <thread>
#include <ctype.h>
#include "ui.cpp"
#include "philosopher.h"
#include <memory>

using namespace std;

int main(int argc, char **argv)
{

    int num_of_philosophers = 5;

    // Fork oznacza w tym przypadku widelec,
    // w żadnym razie nie chodzi o forkowanie procesu
    array<Fork, 5> forks{Fork(), Fork(), Fork(), Fork(), Fork()};

    array<Philosopher, 5> family{
        Philosopher{"Nietchze", forks[0], forks[1]},
        Philosopher{"Descartes", forks[1], forks[2]},
        Philosopher{"Dennett", forks[2], forks[3]},
        Philosopher{"Zizek", forks[3], forks[4]},
        Philosopher{"Plato", forks[4], forks[0]},
    };

    UI ui(family);
    ui.run();

    return 0;
}
