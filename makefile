CC = g++
CFlags = -std=c++17 -pthread -lncurses
Sources = main.cpp philosopher.cpp fork.cpp

all: build

run: build
	./filozofowie

build:
	$(CC) $(Sources) $(CFlags) -o filozofowie

clear:
	rm *.o
