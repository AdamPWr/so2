#include "philosopher.h"
#include <chrono>

void Philosopher::eat()
{

    progress = 0;
    state = WAITING;

    // `scoped_lock` blokuje zasoby tylko w momencie, gdy możliwe jest zablokowanie
    // zarówno prawego, jak i lewego widelca, eliminując ryzyko zakleszczenia.
    scoped_lock lock(forkL._mutex, forkR._mutex);

    // Jak uda się zalockować, zaczynamy jeść
    state = EATING;

    for (int i = 0; i < 20; i++)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        progress += 5;
    }

}

void Philosopher::think()
{
    progress = 0;
    state = SLEEPING;

    static thread_local std::uniform_real_distribution<> dist(2.5, 3.5);
    unsigned sleep_time = dist(generator) * 50;

    for (size_t i = 0; i < 20; i++)
    {
        progress += 5;
        this_thread::sleep_for(chrono::milliseconds(sleep_time));
    }
}

void Philosopher::existance()
{
    while (true)
    {
        if (kill_me)
            break;
        eat();
        think();
    }
}