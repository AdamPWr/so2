#pragma once
#include <thread>
#include "fork.h"
#include <memory>
#include <random>
#include <iostream>
#include <atomic>

using namespace std;

enum PHILOSOPHER_STATE
{
    SLEEPING,
    EATING,
    WAITING
};

class Philosopher
{

    thread lifeThread;
    Fork &forkL;
    Fork &forkR;
    default_random_engine generator;

public:
    string name;
    int progress = 0;
    PHILOSOPHER_STATE state = SLEEPING;

    atomic<bool> kill_me = false;

    Philosopher(const string &p_name, Fork &left, Fork &right) : name(p_name), forkL(left), forkR(right)
    {
        // std::cout<<name<<" Constructor"<<std::endl;
    }

    ~Philosopher()
    {
        if (lifeThread.joinable())
        {
            lifeThread.join();
        }
    }

    void run()
    {
        lifeThread = thread(&Philosopher::existance, this);
    }

    void existance();
    void eat();
    void think();
};
