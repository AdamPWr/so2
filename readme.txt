Problem ucztujących filozofów.

Rozwiązanie zostało zaimplementowane z użyciem `scoped_lock` - systemu zarządzania mutexami,
będącego częścią standardu C++17.

Program zakłada stały czas wykorzystywania zasobów (czas na jedzenie),
oraz losowe przerwy na przetwarzanie (filozofowanie).

Po uruchomieniu, program powinien wyglądać tak jak na obrazku checkme.png

