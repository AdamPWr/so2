#include <ncurses.h>
#include "math.h"
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include "philosopher.h"

#define GREEN_TEXT 1
#define YELLOW_TEXT 2
#define RED_TEXT 3

enum SIMULATION_STATE
{
  PAUSED,
  PLAYING,
  STOPPING
};

class UI
{
public:
  std::array<Philosopher, 5> &school;

  char user_input;

  int X = 0;
  int Y = 0;

  int frame = 0;
  SIMULATION_STATE state = PAUSED;
  bool end = false;

  // Spinning bar
  const char *frame_str[4] = {"\\", "-", "/", "|"};
  const char simulation_state_label[21] = "== Stan symulacji ==";

  char simulation_paused[25] = "Oczekuje na uruchomienie";
  char simulation_playing[14] = "Uruchomiona  ";
  char simulation_stopping[16] = "Zatrzymywanie  ";

  UI(std::array<Philosopher, 5> &p_school) : school(p_school)
  {

  }

  // ncurses specific functions
  void start_bold()
  {
    attron(A_BOLD);
  }

  void end_bold()
  {
    attroff(A_BOLD);
  }

  void init()
  {
    initscr();
    keypad(stdscr, TRUE);
    noecho();

    if (not has_colors())
    {
      printf("Niewłaściwa konfiguracja terminala,\nbrak obsługi kolorów!");
    }

    start_color();
    use_default_colors();

    init_pair(GREEN_TEXT, COLOR_GREEN, -1);
    init_pair(YELLOW_TEXT, COLOR_YELLOW, -1);
    init_pair(RED_TEXT, COLOR_RED, -1);
  }

  void progress_bar(int percentage, int x, int max_x, int y)
  {

    int fill = percentage * (max_x) / 100;

    WINDOW *border_box = newwin(3, max_x + 2, y - 1, x - 1);

    box(border_box, 1, 0);
    wrefresh(border_box);

    int fill_start_x = x;

    int color_pair = GREEN_TEXT;

    if (percentage > 80)
    {
      color_pair = RED_TEXT;
    }
    else if (percentage > 50)
    {
      color_pair = YELLOW_TEXT;
    }

    attron(COLOR_PAIR(color_pair));
    for (int i = 0; i < fill; i++)
    {
      mvprintw(y, i + fill_start_x, "|");
    }

    attroff(COLOR_PAIR(color_pair));
  }

  void philosopher(int y, char *name, PHILOSOPHER_STATE state, int progress_perc)
  {

    mvprintw(y, 2, "Filozof ");
    start_bold();
    mvprintw(y, 11, name);
    end_bold();

    mvprintw(y + 1, 2, "Stan ");
    start_bold();
    if (state == SLEEPING)
      mvprintw(y + 1, 11, "Filozuje");
    else if (state == WAITING)
    {
      attron(COLOR_PAIR(RED_TEXT));
      mvprintw(y + 1, 11, "Czeka");
      attroff(COLOR_PAIR(RED_TEXT));
    }
    else
      mvprintw(y + 1, 11, "Je");

    end_bold();

    progress_bar(progress_perc, 23, X - 25, y + 1);
  }

  void update()
  {

    if (X < 40 || Y < 26)
    {
      attron(COLOR_PAIR(RED_TEXT));
      start_bold();
      mvprintw(0, 0, "Uwaga: Zwieksz rozmiar okna!");
      end_bold();
      attroff(COLOR_PAIR(RED_TEXT));
    }

    int y_position = 1;

    char buffer[20];

    for (int i = 0; i < 5; i++)
    {

      strcpy(buffer, school[i].name.c_str());

      philosopher(y_position, buffer, school[i].state, school[i].progress);
      y_position += 4;
    }

    mvprintw(y_position, (X / 2) - (sizeof(simulation_state_label) / 2), simulation_state_label); //2
    y_position += 1;

    start_bold();

    switch (state)
    {
    case PAUSED:
      mvprintw(y_position, (X / 2) - (sizeof(simulation_paused) / 2), simulation_paused); //2
      break;

    case PLAYING:
      attron(COLOR_PAIR(GREEN_TEXT));
      mvprintw(y_position, (X / 2) - (sizeof(simulation_playing) / 2), simulation_playing);            //2
      mvprintw(y_position, (X / 2) - (sizeof(simulation_playing) / 2) + 12, frame_str[(frame++) % 4]); //2
      attroff(COLOR_PAIR(GREEN_TEXT));
      break;
    case STOPPING:
      attron(COLOR_PAIR(RED_TEXT));
      mvprintw(y_position, (X / 2) - (sizeof(simulation_stopping) / 2), simulation_stopping);           //2
      mvprintw(y_position, (X / 2) - (sizeof(simulation_stopping) / 2) + 12, frame_str[(frame++) % 4]); //2
      attroff(COLOR_PAIR(RED_TEXT));
      break;
    default:
      break;
    }

    end_bold();

    if (user_input == 's' && state == PAUSED)
    {
      state = PLAYING;

      for (int i = 0; i < 5; i++)
      {
        school[i].run();
      }
    }

    // if(user_input == 'w') {
    // state = PAUSED;
    // }

    if (user_input == 'q' || user_input == 'Q')
    {
      state = STOPPING;
      for (auto &member : school)
      {
        member.kill_me = true;
      }
      refresh();
      end = true;
    }

    attron(COLOR_PAIR(YELLOW_TEXT));
    mvprintw(Y - 2, 0, "Pomoc: 'S' aby rozpoczac symulacje, 'Q' aby zakonczyc"); //2
    attroff(COLOR_PAIR(YELLOW_TEXT));

    start_bold();
    mvprintw(Y - 1, 0, "Ilustracja problemu ucztujacych filozofow - Mateusz Baczek, 241330, 2020"); //2
    end_bold();

    if (state == PLAYING)
      frame++;
  }

  void run()
  {

    init();

    do
    {
      clear();
      refresh();
      getmaxyx(stdscr, Y, X); //;

      update();
      timeout(200);
      user_input = getch();

    } while (not end);

    end_ncurses();
  }

  void end_ncurses()
  {
    endwin(); //kończenie
  }
};